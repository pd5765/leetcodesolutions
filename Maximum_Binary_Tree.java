class Solution {
    
    int[] maxNumber(int nums[]){
        int index = 0;
        int value = -1;
        
        for(int i=0;i<nums.length;i++){
            if(value < nums[i]){
                value = nums[i];
                index = i;
            }
        }
        int [] temp = {index, value};
        return temp;
    }
    
    
    TreeNode buildMaxBinaryTree(int []nums){
        
        if(nums.length == 0){
            return null;
        }
        
        int output[] = maxNumber(nums);
       
        TreeNode root = new TreeNode(output[1]);
        
        root.left = buildMaxBinaryTree(Arrays.copyOfRange(nums, 0, output[0]));
        root.right = buildMaxBinaryTree(Arrays.copyOfRange(nums, output[0]+1, nums.length));
        
       return root; 
    }
    
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        
        return buildMaxBinaryTree(nums);
        
    }
}