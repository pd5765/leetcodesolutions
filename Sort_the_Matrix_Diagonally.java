class Solution {
    
    ArrayList<ArrayList<Integer>> sortArr;
    
    public Solution(){
        sortArr = new ArrayList<ArrayList<Integer>>();
    }
    
    public void getElements(int temp[][], int i, int j){
        
        ArrayList<Integer> tempArr = new ArrayList<>();

        while(i< temp.length && j<temp[i].length){
            tempArr.add(temp[i][j]);
            temp[i++][j++] = -1;
        }
        Collections.sort(tempArr);
        sortArr.add(tempArr);
    }
    
    public void sortThisArr(int temp[][]){
        
        for(int i=0; i<temp.length; i++){ 
            for(int j=0; j<temp[i].length; j++){
                if(temp[i][j] == -1){
                    continue;
                }
                getElements(temp, i, j);
            }
        }
    }
    
    public void addElements(int temp[][], int i, int j, ArrayList<Integer> tempArrList){
        int count = 0;
         while(count < tempArrList.size()){
           temp[i++][j++] = tempArrList.get(count++);
        }
    }
    
    public void addThisArr(int mat[][]){
        int counter = 0;
        for(int i=0; i<mat.length; i++){
            
            for(int j=0; j<mat[i].length; j++){
                if(mat[i][j] != -1)
                    continue;
                addElements(mat, i,j, sortArr.get(counter++));
            }
        }
    }
    
    
    public int[][] diagonalSort(int[][] mat) {
        sortThisArr(mat);
        addThisArr(mat);
        return mat;
    }
}