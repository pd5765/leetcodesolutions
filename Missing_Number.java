class Solution {
    public int missingNumber(int[] nums) {
        int maxNumber = nums.length;
        
        int sumOfNumbers = maxNumber * (maxNumber + 1)/2;
        
        for(int i = 0; i < nums.length; i++){
            sumOfNumbers -= nums[i];
        }
        
        return sumOfNumbers;
    }
}