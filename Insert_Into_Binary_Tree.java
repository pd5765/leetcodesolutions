class Solution {
    
    
    void insertIntoTree(TreeNode head, int val){
        
        if(head.val > val){
            if(head.left != null){
                insertIntoTree(head.left, val);    
            }else{
                head.left = new TreeNode(val);
                return;
            }
            
        }else{
            if(head.right != null){
                insertIntoTree(head.right, val);    
            }else{
                head.right = new TreeNode(val);
                return;
            }
        }
    }
    
    public TreeNode insertIntoBST(TreeNode root, int val) {
        if(root != null){
            insertIntoTree(root, val);            
        }else{
            root = new TreeNode(val);
        }
        return root;
    }
}